<?php
/**
 * Config file for Queue
 *
 * PHP version 5
 *
 * @category  Utilities
 * @package   Queue
 * @author    Stefan Majoor <stefan@codeyellow.nl>
 * @copyright 2014 Code Yellow BV
 * @license   MIT License
 * @link      https://bitbucket.org/codeyellow/fuelphp-queue/
 */


return array(


    /*
     * Current active driver (i.e: db = Driver_DB)
     */
    // 'activedriver' => 'db',

    /*
     * list of drivers and their configurations
     */
    // 'drivers' => array(
    //     'db' => array(
    //         'drivername'     => '\\Queue\\Driver_Db',
    //         'tablename'        => 'queue',
    //         'connection'    => false,
    //     ),
    // ),

    /*
    * Not database related config options
    */
    'base_url' => 'queue',    // The url that the queue page is shown upon

    /**
     * If enabled, queues that don't exist will be created if loaded
     */
    'implicit_queue_creation' => true,
    /**
     * If enabled, implicitely created queues will start automatically
     */
    'implicit_queue_start_automatically' => true
);
