<?php
/**
 * Unittests for Core
 *
 * PHP version 5
 *
 * @category  Utilities
 * @package   Queue
 * @author    Stefan Majoor <stefan@codeyellow.nl>
 * @copyright 2014 Code Yellow BV
 * @license   MIT License
 * @link      https://bitbucket.org/codeyellow/fuelphp-queue/
 */

namespace CodeYellow\Queue;

/**
 * @group queue
 * @group queue-core
 */
class Test_Core extends \PHPUnit_Framework_TestCase
{
    static $queue = null;
    static $core = null;

    // Prevents double queues from forming
    public function __construct() 
    {
        static::$core = new Core;

        if (static::$queue !== null) {
            return;
        }

        $queueName = time() . '_Core';
        static::$queue = new Queue();
        static::$queue->create($queueName);
    }

    /**
       * Creates a new job
       * 
       * @return Job a new job
       */
    private static function getNewJob()
    {
        $class = "\Dependency";
        $method = "noException";
        $args = array('testarg' => 'test');
        $queueId = static::$queue->getId();
        $priority = Job::PRIORITY_HIGH;
        $executeAfter = 10;
        $startTime = time();
        $job = new Job;
        return $job->create($class, $method, $args, $queueId, $priority, $executeAfter);
    }

    /**
     * Deletes all the jobs
     *
     * @post all jobs are removed from the jobs table
     */
    private function truncateJobs()
    {
        \DB::delete('queue_errors')
            ->execute();

        \DB::delete('queue_jobs')
            ->execute();
    }


    ///////////////////////////TESTS////////////////////////////////

    // Test if the getJobs function works
    public function testGetJobs()
    {
        $this->truncateJobs();

        $amountTestCases = 5;    // Amount of jobs tested
        $jobLoad = array();        // The jobs as given
        $job = array();            // The actual jobs

        for ($i = $amountTestCases -1; $i >= 0; $i--) {
            $job[$i] = self::getNewJob();
            $jobLoad[$i] = new Job;
        }

        // Changes to job, You can assume that $job->setStatus works
        $job[1]->setStatus(Job::STATUS_DELETED);
        $job[2]->setStatus(Job::STATUS_DONE);


        $me = static::$core->getJobs();

        // Load all the jobs in the array
        // Check if they are equal to the jobs as we set them
        for ($i = $amountTestCases -1; $i >= 0; $i--) {
            $jobLoad[$i]->loadFromArray($me[$i]);
            $this->assertEquals($jobLoad[$i], $job[$i]);
        }
    }

    // Test if the getNextJob returns the correct new Job;
    public function testGetNextJob()
    {
        $this->truncateJobs();
        static::$queue->startQueue();

        // If we add two jobs, execute newest job first
        $job1 = static::getNewJob();
        $job2 = static::getNewJob();
        $nextJob = static::$core->getNextJob();
        $this->assertEquals($job2, $nextJob);

        // Check if a job is done it is not returned anymore
        $job2->setStatus(Job::STATUS_DONE);
        $nextJob = static::$core->getNextJob();
        $this->assertEquals($job1, $nextJob);

        // Check if a job has failed, it is not returned any more
        $job1->setStatus(Job::STATUS_FAILED);
        $nextJob = static::$core->getNextJob();
        $this->assertEquals(null, $nextJob);

        // Check if priority is checked correct
        $job1 = static::getNewJob();
        $job2 = static::getNewJob();
        $job1->setPriority(Job::PRIORITY_LOW);
        $job2->setPriority(Job::PRIORITY_HIGH);
        $nextJob = static::$core->getNextJob();
        $this->assertEquals($job2, $nextJob);

        // Check if only jobs from a queue that is on are returned
        static::$queue->stopQueue();
        $nextJob = static::$core->getNextJob();
        $this->assertEquals(null, $nextJob);

        static::$queue->deleteQueue();
        $nextJob = static::$core->getNextJob();
        $this->assertEquals(null,  $nextJob);

        static::$queue->startQueue();
        $job1->setStatus(Job::STATUS_DONE);
        $job2->setStatus(Job::STATUS_DONE);

        // Check if only jobs that still need to be executed are done
        $job1 = static::getNewJob();
        $job1->setExecuteAfter(time() - 1);
        $nextJob = static::$core->getNextJob();
        $this->assertEquals($job1, $nextJob);

        $job1->setExecuteAfter(time() + 5);
        $nextJob = static::$core->getNextJob();

        $this->assertEquals(null, $nextJob);
    }

    // Test if the executeJob function works
    public function testExecuteJob()
    {
        $job1 = static::getNewJob();
        $job2 = static::getNewJob();
        $job3 = static::getNewJob();

        // A good job should return true;
        $this->assertTrue(static::$core->executeJob($job1));
        $this->assertEquals($job1->getStatus(), Job::STATUS_DONE);


        // // A bad job  should return false
        $job2->setFunction('withException');
        $this->assertFalse(static::$core->executeJob($job2));
        $this->assertEquals($job2->getStatus(), Job::STATUS_FAILED);

        // A non existsent job should return false;
        $job3->setFunction('notExistent');
        $this->assertFalse(static::$core->executeJob($job3));
        $this->assertEquals($job3->getStatus(), Job::STATUS_FAILED);
    }


    // Tests if the threshold works
    public function testThreshold()
    {
        // Set the threshold
        static::$queue->setThreshold(10, 3600)->updateExecuteAfter();


        // Get a new jobs
        $job1 = static::getNewJob();

        // Make sure that job1 is not executed, because of threshold
        $jobnew = static::$core->getNextJob();
        $this->assertEquals(null, $jobnew);

        // Now, delete threshold. Check if this is ok again
        static::$queue->setThreshold(0, 0)->updateExecuteAfter();

        $jobnew = static::$core->getNextJob();
        $this->assertEquals($job1, $jobnew);
    }

    // Test if memory leak does not occur
    public function testMemoryOverflow()
    {
        // Check if a memory leak occurs for good jobs
        $memoryBefore = memory_get_usage(true);
        static::$core->executeJob(new Job(
            '\Dependency',
            'memoryoverflow',
            null,
            static::$queue->getId()
        ));
        $memoryAfter = memory_get_usage(true);
        
        // Check if the memory is cleaned up afterwards
        $this->assertLessThan(
            10,
            $memoryAfter - $memoryBefore,
            "Memory Leak. Memory before: " . $memoryBefore .
            "\nMemory after: " . $memoryAfter 
        );
    }

    // Test the same for jobs that fail
    public function testMemoryOverflowWithException()
    {

        $memoryBefore = memory_get_usage(true);
        static::$core->executeJob(new Job(
            '\Dependency',
            'memoryoverflowWithException',
            null,
            static::$queue->getId()
        ));
        $memoryAfter = memory_get_usage(true);
        
        // Check if the memory is cleaned up afterwards
        $this->assertLessThan(
            10,
            $memoryAfter - $memoryBefore,
            "Memory Leak. Memory before: " . $memoryBefore .
            "\nMemory after: " . $memoryAfter 
        );
    }
}
